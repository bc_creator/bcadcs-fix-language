function buildVersion(v1: number, v2: number, v3: number) {
    return `${v1}.${v2}.${v3}`;
}

export const ModVersion = buildVersion(0, 7, 9);
export const ModName = 'Advanced Drone Control System';

export const ADCS_CUSTOM_ACTION_TAG = 'ADCS_CUSTOM_ACTION_TAG';