import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { DroneName } from "../../../Contents";
import { ChatRoomStdSendAction } from "../../../Locale";
import { CheckOutfitParts, IsInCollar } from "../../../Outift/OutfitCtrl";
import { RestraintCtrl } from "../../../Outift/SetOutfit";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(bind|release) (arms?|legs?|limbs?)", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let target = 'arms';
            let work = 'binded';

            let targetKey: (keyof typeof RestraintCtrl)[] = [];

            const opt_operation = result[1].toLowerCase();

            const opt_target = result[2].toLowerCase();

            if (['arm', 'arms'].includes(opt_target)) {
                target = 'arms';
                targetKey = ['Arms'];
            }
            else if (['leg', 'legs'].includes(opt_target)) {
                target = 'legs';
                targetKey = ['Legs'];
            }
            else if (['limb', 'limbs'].includes(opt_target)) {
                target = 'limbs';
                targetKey = ['Arms', 'Legs'];
            }
            else return;

            let workKey: keyof typeof RestraintCtrl.Arms = 'TurnOn';

            if (['bind'].includes(opt_operation)) {
                work = 'binded';
                workKey = 'TurnOn';
            }
            else if (['release'].includes(opt_operation)) {
                work = 'released';
                workKey = 'TurnOff';
            }
            else return;

            let checkResult = CheckOutfitParts(player, targetKey);
            if (checkResult.length > 0) {
                ChatRoomStdSendAction(`Cannot execute: ${checkResult.join(', ')} is not ADCS item.`);
                return;
            }

            targetKey.forEach(_ => RestraintCtrl[_][workKey](player));
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomStdSendAction(`The ${target} of ${DroneName(player)} is ${work}.`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^at ease", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let checkResult = CheckOutfitParts(player, ['ItemLegs', 'ItemFeet', 'ItemArms']);
            if (checkResult.length > 0) {
                ChatRoomStdSendAction(`Cannot execute: ${checkResult.join(', ')} is not ADCS item.`);
                return;
            }

            RestraintCtrl.Arms.TurnOff(player);
            RestraintCtrl.Legs.TurnOff(player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomStdSendAction(`${DroneName(player)} is released.`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(open|close) (?:chaste|chastity|chastity belt)", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {

            const opt = result[1].toLowerCase();
            let targetKey: keyof typeof RestraintCtrl = "Chaste";

            let checkResult = CheckOutfitParts(player, [targetKey]);
            if (checkResult.length > 0) {
                ChatRoomStdSendAction(`Cannot execute: ${checkResult.join(', ')} is not ADCS item.`);
                return;
            }

            let workDesc = 'opened'
            let workKey: keyof typeof RestraintCtrl.Arms = 'TurnOn';
            if (['open'].includes(opt)) {
                workDesc = 'opened';
                workKey = 'TurnOn';
            }
            else if (['close'].includes(opt)) {
                workDesc = 'closed';
                workKey = 'TurnOff';
            }
            else return;

            RestraintCtrl[targetKey][workKey](player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomStdSendAction(`The chastity of ${DroneName(player)} is ${workDesc}.`);
            return undefined;
        }
    },
    {
        handler: new RegExp("^turn (on|off) (vision|auditory|seeing|hearing)", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let opt_operation = result[1].toLowerCase();
            let opt_target = result[2].toLowerCase();

            let targetDesc = 'vision'
            let targetKey: keyof typeof RestraintCtrl = 'Eyes';
            if (['vision', 'seeing'].includes(opt_target)) {
                targetDesc = 'vision';
                targetKey = 'Eyes';
            }
            else if (['auditory', 'hearing'].includes(opt_target)) {
                targetDesc = 'auditory';
                targetKey = 'Ears';
            }
            else return;

            let checkResult = CheckOutfitParts(player, [targetKey]);
            if (checkResult.length > 0) {
                ChatRoomStdSendAction(`Cannot execute: ${checkResult.join(', ')} is not ADCS item.`);
                return;
            }

            let workDesc = 'turned on'
            let workKey: keyof typeof RestraintCtrl.Arms = 'TurnOn';
            if (['on'].includes(opt_operation)) {
                workDesc = 'turned on';
                workKey = 'TurnOn';
            }
            else if (['off'].includes(opt_operation)) {
                workDesc = 'turned off';
                workKey = 'TurnOff';
            }
            else return;

            RestraintCtrl[targetKey][workKey](player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomStdSendAction(`The ${targetDesc} of ${DroneName(player)} is ${workDesc}.`);
            return undefined;
        }
    },
]

export default CommandList;