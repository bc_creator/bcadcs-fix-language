import { InitiateActTask } from "../../../Task/Task";
import { FindCharacterByName } from "../../../utils/Chat/ChatRoom";
import { CanDoAnyOfActivities } from "../../Activity";
import { AuthorityGreater, AuthorityType, IsModerator } from "../../../Component/Authority";
import { BasicBonusCriteria, CommandUnit } from "../../CommandBasics";

function NameToTarget(ref: string, player: Character, sender: Character) {
    ref = ref.toLowerCase();
    if (ref === 'your') return player;
    else if (ref === 'my') return sender;
    else return FindCharacterByName(ref);
}

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^caress (?:(your|my)|(.+?)\\s*'s) (?:(face|ear|arm|hand|thighs?|shanks?|legs?|vulva|pussy|clit|clitoris|ass|breasts?|nipples?))(?:[\\p{P}\\s]+(.+))?", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let tC = NameToTarget(result[1] || result[2], player, sender);
            if (!tC || !tC.MemberNumber) return;

            const partMap = new Map<string, string[]>([
                ['face', ['ItemHead']],
                ['ear', ['ItemEars']],
                ['arm', ['ItemArms']],
                ['hand', ['ItemHands']],
                ['thighs', ['ItemLegs']],
                ['thigh', ['ItemLegs']],
                ['shanks', ['ItemFeet']],
                ['shank', ['ItemFeet']],
                ['legs', ['ItemLegs', 'ItemFeet']],
                ['leg', ['ItemLegs', 'ItemFeet']],
                ['vulva', ['ItemVulva']],
                ['vulva', ['ItemVulva']],
                ['pussy', ['ItemVulva']],
                ['clit', ['ItemVulvaPiercings']],
                ['clitoris', ['ItemVulvaPiercings']],
                ['ass', ['ItemButt']],
                ['breasts', ['ItemBreast']],
                ['breast', ['ItemBreast']],
                ['nipples', ['ItemNipples']],
                ['nipple', ['ItemNipples']],
            ]);
            let targetBodyPart = partMap.get(result[3].toLowerCase());

            InitiateActTask(
                { TargetPlayer: tC.MemberNumber, TargetActivity: ['Caress'], TargetBodyPart: targetBodyPart },
                BasicBonusCriteria(player, sender),
                IsModerator(player, sender) && CanDoAnyOfActivities(player, tC, ['Caress'], targetBodyPart));

            return result[4];
        }
    },
    {
        handler: new RegExp("^masturbate (?:(your|my)|(.+?)\\s*'s) (?:(pussy|clit|clitoris|ass|breasts?))(?: with (mouth|tongue|finger|hands?|foot|feet))(?:[\\p{P}\\s]+(.+))?", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let tC = NameToTarget(result[1] || result[2], player, sender);
            if (!tC || !tC.MemberNumber) return;

            let targetAct: string[] = [];

            const actMap = new Map<string, string[]>([
                ['mouth', ['MasturbateTongue']],
                ['tongue', ['MasturbateTongue']],
                ['finger', ['MasturbateHand']],
                ['hands', ['MasturbateHand', 'MasturbateFist']],
                ['hand', ['MasturbateHand', 'MasturbateFist']],
                ['fist', ['MasturbateFist']],
                ['fists', ['MasturbateFist']],
                ['feet', ['MasturbateFoot']],
                ['foot', ['MasturbateFoot']],
            ]);

            let act = actMap.get(result[3].toLowerCase()) || [];
            if (act.length === 0) return;
            targetAct = act;

            const partMap = new Map<string, string[]>([
                ['pussy', ['ItemVulva']],
                ['clit', ['ItemVulvaPiercings']],
                ['clitoris', ['ItemVulvaPiercings']],
                ['ass', ['ItemButt']],
                ['breasts', ['ItemBreast']],
                ['breast', ['ItemBreast']],
            ]);
            let targetBodyPart = partMap.get(result[4].toLowerCase());

            InitiateActTask(
                { TargetPlayer: tC.MemberNumber, TargetActivity: targetAct, TargetBodyPart: targetBodyPart },
                BasicBonusCriteria(player, sender),
                IsModerator(player, sender) && CanDoAnyOfActivities(player, tC, targetAct, targetBodyPart));

            return result[5];
        }
    },
    {
        handler: new RegExp("^kiss (?:(your|my)|(.+?)\\s*'s) (?:(foot|feet|shanks?|thighs?|legs?|hands?|arms?|pussy|clit|clitoris|ass|breasts?|nipples?))(?:[\\p{P}\\s]+(.+))?", "iu"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let tC = NameToTarget(result[1] || result[2], player, sender);
            if (!tC || !tC.MemberNumber) return;

            const partMap = new Map<string, string[]>([
                ['foot', ['ItemBoots']],
                ['feet', ['ItemBoots']],
                ['legs', ['ItemFeet', 'ItemLegs']],
                ['leg', ['ItemFeet', 'ItemLegs']],
                ['shanks', ['ItemFeet']],
                ['shank', ['ItemFeet']],
                ['thighs', ['ItemLegs']],
                ['thigh', ['ItemLegs']],
                ['hands', ['ItemHands']],
                ['hand', ['ItemHands']],
                ['arms', ['ItemArms']],
                ['arm', ['ItemArms']],
                ['pussy', ['ItemVulva']],
                ['clit', ['ItemVulvaPiercings']],
                ['clitoris', ['ItemVulvaPiercings']],
                ['ass', ['ItemButt']],
                ['nipples', ['ItemNipples']],
                ['nipple', ['ItemNipples']],
                ['breasts', ['ItemBreast']],
                ['breast', ['ItemBreast']],
            ]);
            let targetBodyPart = partMap.get(result[3].toLowerCase());

            InitiateActTask(
                { TargetPlayer: tC.MemberNumber, TargetActivity: ['Kiss', 'PoliteKiss', 'GaggedKiss'], TargetBodyPart: targetBodyPart },
                BasicBonusCriteria(player, sender),
                IsModerator(player, sender) && CanDoAnyOfActivities(player, tC, ['Kiss', 'PoliteKiss', 'GaggedKiss'], targetBodyPart));

            return result[4];
        }
    },
]


export default CommandList;