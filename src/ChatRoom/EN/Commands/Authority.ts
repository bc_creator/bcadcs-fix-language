import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { GetEffect, SetEffect } from "../../../Component/EffectControl";
import { DroneName } from "../../../Contents";
import { GetData, ServerStoreData } from "../../../Data";
import { ChatRoomStdSendAction } from "../../../Locale";
import { IsInCollar } from "../../../Outift/OutfitCtrl";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(add|remove) moderator[\\p{P}\\s]+(\\d{1,10})", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const opt = result[1].toLowerCase();
            if (opt === 'add') {
                let num = Number.parseInt(result[2]);
                if (GetData().Moderator.includes(num)) {
                    ChatRoomStdSendAction(`${num} has been added to the moderator list of ${DroneName(player)}.`);
                } else {
                    GetData().Moderator.push(num);
                    ServerStoreData();
                    ChatRoomStdSendAction(`${num} is added to the moderator list of ${DroneName(player)}.`);
                }
            }
            else if (opt === 'remove') {
                let num = Number.parseInt(result[2]);
                if (GetData().Moderator.includes(num)) {
                    GetData().Moderator = GetData().Moderator.filter(_ => _ !== num);
                    ServerStoreData();
                    ChatRoomStdSendAction(`${num} is removed from the moderator list of ${DroneName(player)}.`);
                } else {
                    ChatRoomStdSendAction(`${num} is not in the moderator list of ${DroneName(player)}.`);
                }
            }
            return undefined;
        }
    },
    {
        handler: new RegExp("^list moderator", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const loverMsg = GetEffect("LoverNotAsAdmin") ? '' : ' And lovers become moderator automatically.'

            if (GetData().Moderator.length === 0) {
                ChatRoomStdSendAction(`The moderator list of ${DroneName(player)} is empty.${loverMsg}`);
            }
            else ChatRoomStdSendAction(`The moderator list of ${DroneName(player)} as follows: ${GetData().Moderator.join(',')}.${loverMsg}`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(cancel|set) lovers? as moderator", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const opt = result[1].toLowerCase();
            if (opt === 'cancel') {
                SetEffect('LoverNotAsAdmin', true);
            }
            else {
                SetEffect('LoverNotAsAdmin', false);
            }
            ChatRoomStdSendAction(`The lovers of ${DroneName(player)} ${GetEffect("LoverNotAsAdmin") ? 'no longer regarded as' : 'automatically become'} moderator.`);
            return undefined;
        }
    },

]

export default CommandList;