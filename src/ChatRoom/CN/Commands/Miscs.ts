import { DroneName } from "../../../Contents";
import { GetData, WriteData } from "../../../Data";
import { ChatRoomSendAction } from "../../../utils/ChatMessages";
import { IsOwnedBy, IsSaotome, NotBlacklisted, Or } from "../../../utils/Criteria";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(?:导出数据)", "u"),
        validator: [NotBlacklisted, Or(IsOwnedBy, IsSaotome)],
        worker: (player: Character, sender: Character) => {
            let msg = LZString.compressToBase64(JSON.stringify(GetData()));
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 处理数据中。`)
            ServerSend("ChatRoomChat", { Content: msg, Type: "Whisper", Target: sender.MemberNumber });

            return undefined;
        }
    },
    {
        handler: new RegExp("^(?:导入数据)[\\p{P} ]+((?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?)", "u"),
        validator: [NotBlacklisted, Or(IsOwnedBy, IsSaotome)],
        worker: (player: Character, sender: Character, match: RegExpExecArray) => {
            let res = LZString.decompressFromBase64(match[1]);
            if (res === null) return;
            WriteData(JSON.parse(res));
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 处理数据成功。`);

            return undefined;
        }
    },
]

export default CommandList;