import { InitiateActTask } from "../../../Task/Task";
import { FindCharacterByName } from "../../../utils/Chat/ChatRoom";
import { CanDoAnyOfActivities } from "../../Activity";
import { AuthorityGreater, AuthorityType, IsModerator } from "../../../Component/Authority";
import { BasicBonusCriteria, CommandUnit } from "../../CommandBasics";

function NameToTarget(ref: string, player: Character, sender: Character) {
    if (ref === '自己') return player;
    else if (['我', '咱', '俺', '老子'].includes(ref)) return sender;
    else return FindCharacterByName(ref);
}

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^抚摸(.+?)(?:的(脸|耳朵|手臂|手|大腿|小腿|腿部|前阴|下体|阴部|阴蒂|屁股|臀部|乳房|胸部))?(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let tC = NameToTarget(result[1], player, sender);
            if (!tC || !tC.MemberNumber) return;

            const partMap = new Map<string, string[]>([
                ['脸', ['ItemHead']],
                ['耳朵', ['ItemEars']],
                ['手臂', ['ItemArms']],
                ['手', ['ItemHands']],
                ['大腿', ['ItemLegs']],
                ['小腿', ['ItemFeet']],
                ['腿部', ['ItemLegs', 'ItemFeet']],
                ['前阴', ['ItemVulva']],
                ['下体', ['ItemVulva', 'ItemVulvaPiercings']],
                ['阴部', ['ItemVulva', 'ItemVulvaPiercings']],
                ['阴蒂', ['ItemVulvaPiercings']],
                ['臀部', ['ItemButt']],
                ['屁股', ['ItemButt']],
                ['乳房', ['ItemBreast']],
                ['胸部', ['ItemBreast']],
            ]);
            let targetBodyPart = partMap.get(result[2]);

            InitiateActTask(
                { TargetPlayer: tC.MemberNumber, TargetActivity: ['Caress'], TargetBodyPart: targetBodyPart },
                BasicBonusCriteria(player, sender),
                IsModerator(player, sender) && CanDoAnyOfActivities(player, tC, ['Caress'], targetBodyPart));

            return result[3];
        }
    },
    {
        handler: new RegExp("^((?:用(嘴|舌头|拳头|手指|手|脚趾|脚))?(?:抚慰|玩弄)|舔弄)(.+?)(?:的(前阴|下体|阴部|阴蒂|屁股|臀部|乳房|胸部))?(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let tC = NameToTarget(result[3], player, sender);
            if (!tC || !tC.MemberNumber) return;

            let targetAct: string[] = [];
            if (result[1] === '舔弄') {
                targetAct = ['MasturbateTongue', 'Lick'];
            } else if (result[1] === '抚慰' || result[1] === '玩弄') {
                targetAct = ['MasturbateTongue', 'MasturbateHand', 'MasturbateFist', 'MasturbateFoot'];
            } else {
                const actMap = new Map<string, string[]>([
                    ['嘴', ['MasturbateTongue']],
                    ['舌头', ['MasturbateTongue']],
                    ['手指', ['MasturbateHand']],
                    ['手', ['MasturbateHand', 'MasturbateFist']],
                    ['拳头', ['MasturbateFist']],
                    ['脚趾', ['MasturbateFoot']],
                    ['脚', ['MasturbateFoot']],
                ]);

                let act = actMap.get(result[2]) || [];
                if (act.length === 0) return;
                targetAct = act;
            }

            const partMap = new Map<string, string[]>([
                ['前阴', ['ItemVulva']],
                ['下体', ['ItemVulva', 'ItemVulvaPiercings']],
                ['阴部', ['ItemVulva', 'ItemVulvaPiercings']],
                ['阴蒂', ['ItemVulvaPiercings']],
                ['臀部', ['ItemButt']],
                ['屁股', ['ItemButt']],
                ['乳房', ['ItemBreast']],
                ['胸部', ['ItemBreast']],
            ]);
            let targetBodyPart = partMap.get(result[4]);

            InitiateActTask(
                { TargetPlayer: tC.MemberNumber, TargetActivity: targetAct, TargetBodyPart: targetBodyPart },
                BasicBonusCriteria(player, sender),
                IsModerator(player, sender) && CanDoAnyOfActivities(player, tC, targetAct, targetBodyPart));

            return result[5];
        }
    },
    {
        handler: new RegExp("^亲吻?(.+?)(?:的(脚|脚部|腿部|小腿|大腿|手|手部|手臂|阴部|下体|阴蒂|屁股|臀部|小腹|腹部|下腹|乳头|乳房|胸部))?(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let tC = NameToTarget(result[1], player, sender);
            if (!tC || !tC.MemberNumber) return;

            const partMap = new Map<string, string[]>([
                ['脚', ['ItemBoots']],
                ['脚部', ['ItemBoots']],
                ['腿部', ['ItemFeet', 'ItemLegs']],
                ['小腿', ['ItemFeet']],
                ['大腿', ['ItemLegs']],
                ['手', ['ItemHands']],
                ['手部', ['ItemHands']],
                ['手臂', ['ItemArms']],
                ['阴部', ['ItemVulva']],
                ['下体', ['ItemVulva', 'ItemVulvaPiercings']],
                ['阴蒂', ['ItemVulvaPiercings']],
                ['屁股', ['ItemButt']],
                ['臀部', ['ItemButt']],
                ['小腹', ['ItemPelvis']],
                ['腹部', ['ItemPelvis']],
                ['下腹', ['ItemPelvis']],
                ['乳头', ['ItemNipples']],
                ['乳房', ['ItemBreast']],
                ['胸部', ['ItemNipples', 'ItemBreast']],
            ]);
            let targetBodyPart = partMap.get(result[2]);

            InitiateActTask(
                { TargetPlayer: tC.MemberNumber, TargetActivity: ['Kiss', 'PoliteKiss', 'GaggedKiss'], TargetBodyPart: targetBodyPart },
                BasicBonusCriteria(player, sender),
                IsModerator(player, sender) && CanDoAnyOfActivities(player, tC, ['Kiss', 'PoliteKiss', 'GaggedKiss'], targetBodyPart));

            return result[3];
        }
    },
]


export default CommandList;