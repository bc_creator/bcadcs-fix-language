import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { GetEffect, SetEffect } from "../../../Component/EffectControl";
import { GetOwnerRef, GetSelfRef, SetOwnerRef, SetSelfRef } from "../../../Component/RefGame";
import { DroneName } from "../../../Contents";
import { defaultResponseTime, GetData, ServerStoreData } from "../../../Data";
import { ModVersion } from "../../../Definitions";
import { InjectFullbodyWithReport, IsInCollar, ItemDescritpion, ReColorItems } from "../../../Outift/OutfitCtrl";
import { InitiateTalkTask } from "../../../Task/Task";
import { ChatRoomSendAction } from "../../../utils/ChatMessages";
import { IsOwnedBy } from "../../../utils/Criteria";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(取消)?(?:设定自称)[\\p{P}\\s~]*(.*)", "u"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let old = GetSelfRef(player);
            if (result[1]) {
                SetSelfRef(player, '');
            }
            else {
                if (!result[2]) return;
                let re = result[2].trim();
                if (!re) return;
                SetSelfRef(player, re);
            }
            let targetMessage = `${old}现在起将以${GetSelfRef(player)}自称。`;
            InitiateTalkTask(targetMessage, IsOwnedBy(player, sender));

            return undefined;
        }
    },
    {
        handler: new RegExp("^(取消)?(?:设定主人称呼)[\\p{P}\\s~]*(.*)", "u"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetOwnerRef(player, '');
            }
            else {
                if (!result[2]) return;
                let re = result[2].trim();
                if (!re) return;
                SetOwnerRef(player, re);
            }
            let targetMessage = `${GetSelfRef(player)}现在起将以${GetOwnerRef(player)}称呼主人。`;
            InitiateTalkTask(targetMessage, IsOwnedBy(player, sender));

            return undefined;
        }
    },
    {
        handler: new RegExp("^(打开|关闭)发言辅助", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let targetEffect: EffectType = 'NoTalkGuide';

            const OnList = ['开启', '启动', '打开', '解开'];
            const OffList = ['关闭', '封闭', '关掉'];
            const SwitchList = ['转换', '切换'];

            let tOnlist = OnList.concat(SwitchList);
            let tOfflist = OffList.concat(SwitchList);

            if (result[1]) {
                if (GetData().Effects.includes(targetEffect)) {
                    if (tOnlist.includes(result[1])) {
                        GetData().Effects = GetData().Effects.filter(_ => _ !== targetEffect);
                    }
                }
                else if (tOfflist.includes(result[1])) {
                    GetData().Effects.push(targetEffect);
                }
            }

            CharacterRefresh(player);
            ServerStoreData();
            if (GetData().Effects.includes(targetEffect)) {
                ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的发言辅助已关闭。`);
            }
            else {
                ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的发言辅助已开启。`);
            }

            return undefined;
        }
    },
    {
        handler: new RegExp("^(?:设置|配置|设定)主题(?:颜色)?[\\p{P}\\s]+#([0-9A-F]{6}|[0-9A-F]{3})", "iu"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            GetData().Theme.Color = `#${result[1]}`;
            ServerStoreData();
            ReColorItems(player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomSendAction(`[ADCS] 已经配置主题颜色：#${result[1]}`);
            return undefined;
        }
    },
    {
        handler: new RegExp("^注入(?:穿着|穿戴)(?:物品|道具|装备)", "u"),
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let v = InventoryGet(player, 'ItemNeck');
            let r = ItemDescritpion(player, 'ItemNeck');

            if (r === undefined) return;

            if (!v || v.Asset.Name !== r.Name) {
                ChatRoomSendAction(`[ADCS] 必须穿戴${r.Description}才能继续。`);
                return;
            }

            let report = InjectFullbodyWithReport(player);

            let injected = report.filter(_ => _.State === 'Injected');
            let changMessage = injected.length > 0 ? `已对${injected.map(_ => _.GroupDescription).join(`、`)}穿着物品注入控制。` : `未进行进一步的控制。`;

            let tobedone = report.filter(_ => _.State === 'ToBeDone');
            let firstTwo = tobedone.slice(0, 2);
            let toDoMessage = tobedone.length > 0 ? `在${firstTwo.map(_ => _.GroupDescription).join(`、`)}分别穿戴${firstTwo.map(_ => _.Description).join('、')}可以进一步注入控制。` : `已经完成所有可能的控制注入。`;

            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);

            ChatRoomSendAction(`[ADCS] ${changMessage}${toDoMessage}`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(取消)?(?:设(?:定|置)(?:反应|回应)时间)[\\p{P}\\s~]*(\\d+(?:\\.\\d+)?)?", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().ADCS.ResponseTime = defaultResponseTime;
            }
            else {
                let v = Number(result[2])
                GetData().ADCS.ResponseTime = Math.floor(v * 1000);
            }
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 的反应时间设定为 ${GetData().ADCS.ResponseTime / 1000} 秒。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(停止|取消|不再)?(?:禁足|禁止离开|禁止走动)", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetEffect('Grounded', false);
            } else {
                SetEffect('Grounded', true);
                ChatRoomSlowStop = true;
            }
            ServerStoreData();
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 现在${GetEffect('Grounded') ? '已经' : '不再'}被禁足。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(取消)?(?:注入GGTS)", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().Effects = GetData().Effects.filter(_ => _ !== 'InjectGGTS');
            } else if (!GetData().Effects.includes('InjectGGTS')) {
                GetData().Effects.push('InjectGGTS');
            }
            ServerStoreData();
            ChatRoomSendAction(`[ADCS] ${DroneName(player)} 现在${GetData().Effects.includes('InjectGGTS') ? '已经' : '未'}注入GGTS系统。`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(?:维护模式|(?:汇报)?状态)", "u"),
        validator: [AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character) => {
            if (IsInCollar(player)) {
                ServerStoreData();
                let State = [
                    GetEffect('PunishOrgasm') ? '惩罚高潮' : '',
                    GetEffect('NoTalkGuide') ? '无发言辅助' : '',
                    GetEffect('InjectGGTS') ? '注入GGTS' : '',
                    GetEffect('AwardFullGear') ? '奖励全套装备' : '',
                    GetEffect('Grounded') ? '禁足' : '',
                    GetEffect('LoverNotAsAdmin') ? '' : '恋人是管理员',
                ].filter(_ => _);

                const verMsg = `先进仆从机控制系统™ 版本 v${ModVersion}。`;
                const ctrlMsg = `控制对象：${player.MemberNumber}。`;
                const bpMsg = `奖励点数：${GetData().ADCS.Score}，惩罚点数：${GetData().ADCS.Punish}。`;
                const stateMsg = `当前状态：${State.length > 0 ? State.join('，') : '无'}。`;

                ChatRoomSendAction(`[ADCS] ${verMsg}${ctrlMsg}${bpMsg}${stateMsg}`);
            }
            else {
                ChatRoomSendAction(`[ADCS] ADCS™中央处理器不在线，请正确注入项圈控制。`);
            }

            return undefined;
        }
    },
]

export default CommandList;