import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { GetEffect, SetEffect } from "../../../Component/EffectControl";
import { DroneName } from "../../../Contents";
import { GetData, ServerStoreData } from "../../../Data";
import { IsInCollar } from "../../../Outift/OutfitCtrl";
import { ChatRoomSendAction } from "../../../utils/ChatMessages";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(添加|移除)管理员[\\p{P}\\s]+(\\d{1,10})", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1] === '添加') {
                let num = Number.parseInt(result[2]);
                if (GetData().Moderator.includes(num)) {
                    ChatRoomSendAction(`[ADCS] ${num}已经存在于${DroneName(player)}的管理员列表中。`);
                } else {
                    GetData().Moderator.push(num);
                    ServerStoreData();
                    ChatRoomSendAction(`[ADCS] ${num}已经添加到${DroneName(player)}的管理员列表中。`);
                }
            }
            else if (result[1] === '移除') {
                let num = Number.parseInt(result[2]);
                if (GetData().Moderator.includes(num)) {
                    GetData().Moderator = GetData().Moderator.filter(_ => _ !== num);
                    ServerStoreData();
                    ChatRoomSendAction(`[ADCS] ${num}已经从${DroneName(player)}的管理员列表中移除。`);
                } else {
                    ChatRoomSendAction(`[ADCS] ${num}不存在${DroneName(player)}的管理员列表中。`);
                }
            }
            return undefined;
        }
    },
    {
        handler: new RegExp("^列出管理员", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const loverMsg = GetEffect("LoverNotAsAdmin") ? '' : '且爱人自动成为管理员。'

            if (GetData().Moderator.length === 0) {
                ChatRoomSendAction(`[ADCS] ${DroneName(player)}的管理员列表为空。${loverMsg}`);
            }
            else ChatRoomSendAction(`[ADCS] ${DroneName(player)}的管理员列表如下：${GetData().Moderator.join('、')}。${loverMsg}`);

            return undefined;
        }
    },
    {
        handler: new RegExp("^(取消)?(?:设置|设定)(?:恋人|爱人)为管理员", "u"),
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1] === '取消') {
                SetEffect('LoverNotAsAdmin', true);
            }
            else {
                SetEffect('LoverNotAsAdmin', false);
            }
            ChatRoomSendAction(`[ADCS] ${DroneName(player)}的爱人${GetEffect("LoverNotAsAdmin") ? '不再' : '会'}自动成为管理员。`);
            return undefined;
        }
    },

]

export default CommandList;