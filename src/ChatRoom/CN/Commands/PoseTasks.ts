import { CheckPose, InitiatePoseTask } from "../../../Task/Task";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { BasicBonusCriteria, BasicPosePenaltyCriteria, CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: new RegExp("^(?:举起双?手|手举起来)(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            const target = ['Yoked', 'OverTheHead'];
            if (CheckPose(player, target)) return;
            InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));

            return result[1];
        }
    },
    {
        handler: new RegExp("^手放(?:到|在)(身后|背后|身前)(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (['身后', '背后'].includes(result[1])) {
                const target = ['BackBoxTie', 'BackElbowTouch', 'BackCuffs'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['身前'].includes(result[1])) {
                const target = ['BaseUpper'];
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
    {
        handler: new RegExp("^(站立|站着|站起来|跪下|跪着)(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (['站立', '站着', '站起来'].includes(result[1])) {
                const target = ['BaseLower', 'LegsClosed'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['跪下', '跪着'].includes(result[1])) {
                const target = ['KneelingSpread', 'Kneel'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
    {
        handler: new RegExp("^(?:双?腿(分开|打开|并拢|合并)|(分开|打开|并拢|合并)双?腿)(?:[\\p{P}\\s]+(.+))?", "u"),
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let opt = result[1] || result[2] || "";
            if (['打开', '分开'].includes(opt)) {
                const target = ['Spread', 'KneelingSpread', 'BaseLower'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['并拢', '合并'].includes(opt)) {
                const target = ['LegsClosed', 'Kneel'];
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[3];
        }
    },
]

export default CommandList;