import { GetData } from "../Data";
import { GetString } from "../Locale";
import { CollarItem, OutfitItemMap, OutfitItems, OutfitItemType } from "./OutfitDefinition";

export function CheckOutfit(itemE: OutfitItemType, itemV: Item): boolean {
    return itemE.Asset.Name === itemV.Asset.Name
        && itemE.Asset.Group === itemV.Asset.Group.Name
        && itemV.Craft !== undefined && itemE.Craft().Name === itemV.Craft.Name;
}

export function CheckOutfitWithSwitch(C: Character, target: string | Item) {
    const not_match = { then: () => false };
    if (typeof target === 'string') {
        const v = InventoryGet(C, target);
        if (v === null) return not_match;
        target = v;
    }
    let vE = OutfitItemMap.get(target.Asset.Group.Name);
    if (vE === undefined) return not_match;
    if (!CheckOutfit(vE, target)) return not_match;
    return {
        then: (f: (iV: Item, iE: OutfitItemType) => void) => f(target as Item, vE as OutfitItemType),
    };
}

export function IsInFullOutfit(player: Character): boolean {
    const itemM = OutfitItemMap;
    const partOfOutfit = player.Appearance.filter(_ => {
        if (_.Asset.Group.Category !== "Item") return false;
        const T = itemM.get(_.Asset.Group.Name);
        if (T === undefined) return false;
        return CheckOutfit(T, _);
    });

    return partOfOutfit.length === OutfitItemMap.size;
}

export function IsInCollar(player: Character): boolean {
    const neck = InventoryGet(player, 'ItemNeck');
    return neck !== null && CheckOutfit(CollarItem, neck);
}

export function IsTargetItemInOutfit(player: Character, itemE: OutfitItemType): boolean {
    const itemV = InventoryGet(player, itemE.Asset.Group);
    return itemV !== null && itemE !== undefined && CheckOutfit(itemE, itemV);
}

export function IsTargetGroupInOutfit(player: Character, group: string): boolean {
    const itemV = InventoryGet(player, group);
    const itemE = OutfitItemMap.get(group);
    return itemV !== null && itemE !== undefined && CheckOutfit(itemE, itemV);
}

export function InjectFullbodyWithReport(player: Character): (ItemDescritpionType & { State: "Finished" | "Injected" | "ToBeDone" })[] {
    let ret = [] as (ItemDescritpionType & { State: "Finished" | "Injected" | "ToBeDone" })[];

    let group_set = new Set(OutfitItems.map(_ => _.Asset.Group));

    ret = player.Appearance.map(_ => {
        const iE = OutfitItemMap.get(_.Asset.Group.Name);
        let State = 'ToBeDone' as typeof ret[0]['State'];

        if (iE === undefined) return undefined;

        const isFinished = CheckOutfit(iE, _);

        if (isFinished) {
            group_set.delete(_.Asset.Group.Name);
            State = 'Finished';
        }
        else {
            if (iE.Asset.Name === _.Asset.Name) {
                if (InjectTargetItem(player, _)) {
                    group_set.delete(_.Asset.Group.Name);
                    State = 'Injected';
                }
            }
        }

        return {
            State,
            ...ItemDescritpion(player, iE)
        }
    }).concat(Array.from(group_set).map(_ => {
        const iE = OutfitItemMap.get(_);

        return iE && {
            State: 'ToBeDone',
            ...ItemDescritpion(player, iE)
        }
    })).filter(_ => _ !== undefined) as typeof ret;
    return ret;
}

export function InjectTargetItem(player: Character, itemV: Item, itemE?: OutfitItemType) {
    if (!itemE) itemE = OutfitItemMap.get(itemV.Asset.Group.Name);
    if (itemE === undefined) return false;
    if (!player.Ownership || !player.Ownership.MemberNumber) return false;

    if (itemV.Craft !== undefined) return false;
    if (itemV.Property && itemV.Property.LockedBy) return false;

    const theme = GetData().Theme.Color;

    itemV.Color = itemE.Color(theme);
    itemV.Craft = {
        ...(itemE.Craft()),
        Item: itemE.Asset.Name,
        MemberName: player.Ownership.Name,
        MemberNumber: player.Ownership.MemberNumber,
        Color: itemV.Color.join(',')
    };

    if (itemE.Property) {
        itemV.Property = itemE.Property();
        if (itemV.Asset.Extended)
            ExtendedItemInit(player, itemV);
    }

    InventoryLock(player, itemV, itemV.Craft.Lock, player.Ownership.MemberNumber);

    return true;
}

export function ReColorItems(player: Character) {
    if (!player.Ownership || !player.Ownership.MemberNumber) return;

    const theme = GetData().Theme.Color;

    player.Appearance.forEach(_ => {
        const iE = OutfitItemMap.get(_.Asset.Group.Name);
        if (iE === undefined) return;

        const color = iE.Color(theme);

        _.Color = color;
        if (_.Craft) _.Craft.Color = color.join(',');
    });
}

export function ListInjectables(player: Character) {
    const finished = new Set<string>();

    player.Appearance.forEach(_ => {
        const itE = OutfitItemMap.get(_.Asset.Group.Name);
        if (itE !== undefined && CheckOutfit(itE, _)) finished.add(_.Asset.Group.Name);
    });

    return OutfitItems.filter(_ => !finished.has(_.Asset.Group)).map(_ => _.Asset);
}

interface ItemDescritpionType {
    Name: string,
    Description: string,
    GroupDescription: string
}

export function ItemDescritpion(C: Character, outfitAsset: OutfitItemType | string): ItemDescritpionType | undefined {
    if (typeof outfitAsset === 'string') outfitAsset = OutfitItemMap.get(outfitAsset) as OutfitItemType;

    const target = AssetGet(C.AssetFamily, outfitAsset.Asset.Group, outfitAsset.Asset.Name);

    if (!target) return undefined;

    let ret: ItemDescritpionType = {
        Name: target.Name,
        Description: target.Description,
        GroupDescription: target.Group.Description
    }

    if (outfitAsset.Asset.OverrideGroupDescription) {
        ret.GroupDescription = GetString(outfitAsset.Asset.OverrideGroupDescription as any);
    }

    return ret;
}



export function CheckOutfitParts(C: Character, Groups: string[]): ItemDescritpionType[] {
    return Groups.filter(_ => OutfitItemMap.has(_) && !IsTargetGroupInOutfit(C, _)).map(_ => {
        return ItemDescritpion(C, OutfitItemMap.get(_) as OutfitItemType);
    }).filter(_ => _ !== undefined) as ItemDescritpionType[];
}