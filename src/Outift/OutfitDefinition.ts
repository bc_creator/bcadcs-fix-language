import { GetString } from "../Locale"

export type OutfitItemType = {
    Asset: {
        Name: string,
        Group: string,
        OverrideGroupDescription?: string,
    },
    Color: ((theme: string) => string[]),
    Craft: (() => {
        Name: string,
        Description: string,
        Lock: string,
        Property: string,
        OverridePriority: number | null,
        Private: true,
        Type?: string,
    }),
    Property?: (() => ItemProperty) | undefined

}

export const OutfitItems: OutfitItemType[] = [
    {
        Asset: {
            Name: "FuturisticCollar",
            Group: "ItemNeck"
        },
        Color: (theme: string) => ["#DDDDDD", theme, theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_collar_name"),
                Description: GetString("outfit_collar_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticHarness",
            Group: "ItemTorso"
        },
        Color: (theme: string) => ["#DDDDDD", theme, theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_harness_name"),
                Description: GetString("outfit_harness_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticChastityBelt",
            Group: "ItemPelvis"
        },
        Color: (theme: string) => [theme, "#DDDDDD", theme, "#000000", theme, theme, theme, theme, theme],
        Craft: () => {
            return {
                Name: GetString("outfit_chastity_name"),
                Description: GetString("outfit_chastity_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        },
        Property: () => {
            return {
                Block: [],
                Type: "m3f0b0t0o0"
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticBra",
            Group: "ItemBreast"
        },
        Color: (theme: string) => ["#DDDDDD", "#000000", "#939393", theme, theme],
        Craft: () => {
            return {
                Name: GetString("outfit_bra_name"),
                Description: GetString("outfit_bra_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
                Type: "Show2",
            }
        },
        Property: () => {
            return {
                Type: "Show2"
            }
        }
    },
    {
        Asset: {
            Name: "InteractiveVisor",
            Group: "ItemHead"
        },
        Color: (theme: string) => [theme],
        Craft: () => {
            return {
                Name: GetString("outfit_visor_name"),
                Description: GetString("outfit_visor_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticEarphones",
            Group: "ItemEars"
        },
        Color: (theme: string) => [theme, "#DDDDDD", "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_ear_name"),
                Description: GetString("outfit_ear_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticCuffs",
            Group: "ItemArms"
        },
        Color: (theme: string) => ["#DDDDDD", theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_arms_name"),
                Description: GetString("outfit_arms_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticMittens",
            Group: "ItemHands"
        },
        Color: (theme: string) => [theme, "#FFFFFF", theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_hands_name"),
                Description: GetString("outfit_hands_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticLegCuffs",
            Group: "ItemLegs",
            OverrideGroupDescription: "outfit_group_name_legs"
        },
        Color: (theme: string) => [theme, "#FFFFFF", theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_thighs_name"),
                Description: GetString("outfit_thighs_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticAnkleCuffs",
            Group: "ItemFeet",
            OverrideGroupDescription: "outfit_group_name_feet"
        },
        Color: (theme: string) => [theme, "#DDDDDD", theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_shanks_name"),
                Description: GetString("outfit_shanks_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
    {
        Asset: {
            Name: "FuturisticHeels2",
            Group: "ItemBoots"
        },
        Color: (theme: string) => [theme, "#DDDDDD", "Default", theme, theme, theme, "#000000"],
        Craft: () => {
            return {
                Name: GetString("outfit_feet_name"),
                Description: GetString("outfit_feet_desc"),
                Lock: "OwnerPadlock",
                Property: "Secure",
                OverridePriority: null,
                Private: true,
            }
        }
    },
]

export const OutfitItemMap = new Map<string, OutfitItemType>(OutfitItems.map(_ => [_.Asset.Group, _]));

export const CollarItem = OutfitItems.find(_ => _.Asset.Group === 'ItemNeck') as OutfitItemType;

export function GetOutfitItemByGroup(group: string) {
    return OutfitItemMap.get(group);
}