import { CheckOutfit, CheckOutfitWithSwitch, IsTargetGroupInOutfit, IsTargetItemInOutfit } from "./OutfitCtrl";
import { OutfitItemMap, OutfitItemType } from "./OutfitDefinition";

function ItemSetTypeForGroup(C: Character, Options: ExtendedItemOption[], Option: ExtendedItemOption, group: AssetGroup) {
    const old_grp = C.FocusGroup;
    C.FocusGroup = group;
    ExtendedItemSetType(C, Options, Option);
    C.FocusGroup = old_grp;
}

function SetOptionForItem(player: Character, group: string, option: string) {
    const target_group = AssetGroupGet(player.AssetFamily, group);
    const target_itemE = OutfitItemMap.get(group);
    if (target_group === null || target_itemE === undefined || !IsTargetItemInOutfit(player, target_itemE)) return;

    const options = AssetFemale3DCGExtended[target_group.Name][target_itemE.Asset.Name]['Config']['Options'] as ExtendedItemOption[]
    ItemSetTypeForGroup(player, options, options.find(_ => _.Name === option) as ExtendedItemOption, target_group);
}

export const RestraintCtrl = {
    Legs: {
        TurnOn: (player: Character) => SetLegs(player, true),
        TurnOff: (player: Character) => SetLegs(player, false),
    },
    Arms: {
        TurnOn: (player: Character) => SetArms(player, true),
        TurnOff: (player: Character) => SetArms(player, false),
    },
    Ears: {
        TurnOn: (player: Character) => SetHearing(player, false),
        TurnOff: (player: Character) => SetHearing(player, true),
    },
    Eyes: {
        TurnOn: (player: Character) => SetVision(player, false),
        TurnOff: (player: Character) => SetVision(player, true),
    },
    Chaste: {
        TurnOn: (player: Character) => SetChastity(player, false),
        TurnOff: (player: Character) => SetChastity(player, true),
    },
}

function SetGroupProperty(player: Character, itemV: Item, itemE: OutfitItemType, property: ItemProperty) {
    const lockP = itemV.Property ? InventoryExtractLockProperties(itemV.Property) : undefined;
    if (lockP) {
        itemV.Property = Object.assign(JSON.parse(JSON.stringify(property)) as ItemProperty, lockP)
        if (!itemV.Property.Effect) itemV.Property.Effect = [];
        itemV.Property.Effect.push('Lock');
    }
    else {
        itemV.Property = JSON.parse(JSON.stringify(property)) as ItemProperty;
    }
}

function GetPropertyFromAssetExtended(itemE: OutfitItemType, option: string) {
    const exItem = (AssetFemale3DCGExtended[itemE.Asset.Group][itemE.Asset.Name]['Options'] as ExtendedItemOption[]).find(_ => _.Name === option);
    if (!exItem) return {};
    return {...exItem.Property, Type:option === 'None'? null : option};
}

export function SetLegs(player: Character, isTied: boolean) {

    if (isTied) {
        CheckOutfitWithSwitch(player, 'ItemLegs').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'Closed'));
        });
        CheckOutfitWithSwitch(player, 'ItemFeet').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'Closed'));
        });
    }
    else {
        CheckOutfitWithSwitch(player, 'ItemLegs').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'None'));
        });
        CheckOutfitWithSwitch(player, 'ItemFeet').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'None'));
        });
    }
}

export function SetArms(player: Character, isTied: boolean) {
    if (isTied) {
        CheckOutfitWithSwitch(player, 'ItemArms').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'Both'));
        });
    }
    else {
        CheckOutfitWithSwitch(player, 'ItemArms').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'None'));
        });
    }
}

export function SetHearing(player: Character, isDeafened: boolean) {
    if (isDeafened) {
        CheckOutfitWithSwitch(player, 'ItemEars').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'NoiseCancelling'));
        });
    } else {
        CheckOutfitWithSwitch(player, 'ItemEars').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'Off'));
        });
    }
}

export function SetVision(player: Character, isBlinded: boolean) {
    if (isBlinded) {
        CheckOutfitWithSwitch(player, 'ItemHead').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'Blind'));
        });
    } else {
        CheckOutfitWithSwitch(player, 'ItemHead').then((iV, iE) => {
            SetGroupProperty(player, iV, iE, GetPropertyFromAssetExtended(iE, 'Transparent'));
        });
    }
}

export function SetChastity(player: Character, isChasted: boolean) {
    const target_group = AssetGroupGet(player.AssetFamily, 'ItemPelvis');
    const target_itemE = OutfitItemMap.get('ItemPelvis');
    const target_itemV = InventoryGet(player, 'ItemPelvis');
    if (target_itemV === null || target_group === null || target_itemE === undefined || !CheckOutfit(target_itemE, target_itemV)) return;
    const old_type = target_itemV.Property?.Type;

    const target = isChasted ? `f1b1` : `f0b0`;

    const newType = old_type ? `${old_type.substring(0, 2)}${target}${old_type.substring(6)}` : `m3${target}t0o0`

    const target_property = isChasted ? {
        Block: ["ItemVulva", "ItemVulvaPiercings", "ItemButt"],
        Effect: ["Chaste"],
        Type: newType,
    } : { Block: [], Effect: [], Type: newType };

    target_itemV.Property = (() => {
        if (target_itemV.Property === undefined) {
            return target_property;
        }
        const new_property = InventoryExtractLockProperties(target_itemV.Property);
        if (!new_property.Block) new_property.Block = []
        new_property.Block.push(...(target_property.Block));

        if (!new_property.Effect) new_property.Effect = []
        new_property.Effect.push(...(target_property.Effect));

        if (new_property.LockedBy)
            new_property.Effect.push('Lock');

        new_property.Type = newType;
        return new_property;
    })();
}