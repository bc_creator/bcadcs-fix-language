// ==UserScript==
// @name BC Advanced Drone Control System
// @namespace https://www.bondageprojects.com/
// @version 1
// @description BC Advanced Drone Control System
// @author Saki Saotome
// @include      /^https:\/\/(www\.)?bondage(?:projects\.elementfx|-europe)\.com\/R\d+\/(BondageClub|\d+)(\/)?(((index|\d+)\.html)?)?$/
// @icon         https://dynilath.gitlab.io/SaotomeToyStore/favicon.ico
// @grant none
// @run-at document-end
// ==/UserScript==

(function () {
    "use strict";
    if (typeof BCADCS_Loaded === "undefined") {
        const script = document.createElement("script");
        script.src = `https://dynilath.gitlab.io/SaotomeToyStoreVendor/ADCS/main.js?v=${Date.now()}`;
        document.head.appendChild(script);
    }
})();
