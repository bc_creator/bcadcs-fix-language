import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { IsTargetGroupInOutfit } from "../Outift/OutfitCtrl";

type OrgasmEvent = { Orgasm: (C: Character) => void, Ruined: (C: Character) => void, Resisted: (C: Character) => void };

function OrgasmHandler(C: Character, Progress: number, event: OrgasmEvent) {
    if (Player === undefined || Player.MemberNumber === undefined) return;
    if (C.MemberNumber !== Player.MemberNumber) return;
    if (Progress < 25) event.Orgasm(C);
    else if (ActivityOrgasmRuined) event.Ruined(C);
    else event.Resisted(C);
}

export function hookOrgasmEvent(mod: ModSDKModAPI<any>, event: OrgasmEvent) {
    mod.hookFunction('ActivityOrgasmStop', 9, (args, next) => {
        next(args);
        if (Player && IsTargetGroupInOutfit(Player, 'ItemNeck'))
            OrgasmHandler(args[0] as Character, args[1] as number, event);
    });
}
