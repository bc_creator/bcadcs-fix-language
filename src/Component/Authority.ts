import { GetData } from "../Data";
import { NotSelf } from "../utils/Criteria";
import { GetEffect } from "./EffectControl";

export enum AuthorityType {
    Allowed,
    Moderator,
    Owner
}

function NotBlackListed(player: Character, C: Character) {
    return (player.BlackList && (!player.BlackList.includes(C.MemberNumber as number))) || IsModerator(player, C);
}

export function IsModerator(player: Character, C: Character) {
    if (GetData().Moderator.includes(C.MemberNumber as number)) return true;
    if (!GetEffect("LoverNotAsAdmin") && player.Lovership && player.Lovership.find(_ => _.MemberNumber === C.MemberNumber)) return true;
    if (IsOwner(player, C)) return true;
    return false;
}

export function IsOwner(player: Character, C: Character) {
    return player.IsOwnedByMemberNumber(C.MemberNumber as number);
}

function BasicFilter(criteria: (player: Character, C: Character) => boolean) {
    return (player: Character, C: Character) => {
        if (player.MemberNumber === undefined || C.MemberNumber === undefined) return false;
        if (player.MemberNumber === C.MemberNumber) return false;
        return criteria(player, C);
    }
}

function AvoidSelf(criteria: (player: Character, C: Character) => boolean) {
    return (player: Character, C: Character) => NotSelf(player, C) && criteria(player, C);
}

export function AuthorityGreater(auth: AuthorityType) {
    switch (auth) {
        case AuthorityType.Allowed:
            return AvoidSelf(NotBlackListed);
        case AuthorityType.Moderator:
            return AvoidSelf(IsModerator);
        case AuthorityType.Owner:
            return AvoidSelf(IsOwner);
    }
}