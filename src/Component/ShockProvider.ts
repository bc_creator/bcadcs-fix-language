import { Monitor } from ".././utils/Monitor";
import { ChatRoomStdLocalAction, GetString } from "../Locale";
import { CheckOutfitWithSwitch } from "../Outift/OutfitCtrl";

export function queueShockAction(count: number) {
    ShockUnit.instance.counter += count;
}

function IsSimpleChat(msg: string) {
    return msg.trim().length > 0 && !msg.startsWith("/") && !msg.startsWith("(") && !msg.startsWith("*");
}

function ChatRoomInterceptMessage(cur_msg: string, msg: string) {
    if (!cur_msg) return;
    ElementValue("InputChat", cur_msg + "... " + msg);
    ChatRoomSendChat();
}

export class ShockUnit {
    private static _instance: ShockUnit;

    static get instance() {
        if (this._instance == null)
            this._instance = new ShockUnit;
        return this._instance
    }

    private single(player: Character, Item: Item) {
        if (!player.MemberNumber) return;

        if (CurrentCharacter !== null)
            CurrentCharacter.FocusGroup = null;
        CurrentCharacter = null;
        if (CurrentScreen === 'Preference')
            CommonSetScreen("Character", "InformationSheet");
        if (CurrentScreen === 'ChatAdmin')
            CommonSetScreen('Online', 'ChatRoom');
        if (CurrentScreen === 'InformationSheet')
            CommonSetScreen(InformationSheetPreviousModule, InformationSheetPreviousScreen);

        let msg = ElementValue("InputChat");
        if (IsSimpleChat(msg))
            ChatRoomInterceptMessage(msg, '');

        const MemberNumber = player.MemberNumber;
        const intensity = 2;

        const Dictionary = [
            { Tag: "DestinationCharacterName", Text: CharacterNickname(player), MemberNumber: MemberNumber },
            { Tag: "AssetName", AssetName: Item.Asset.Name, GroupName: Item.Asset.Group.Name },
            { ShockIntensity: intensity * 1.5 },
            { FocusGroupName: Item.Asset.Group.Name },
        ];

        if (CurrentScreen == "ChatRoom")
            ServerSend("ChatRoomChat", { Content: 'TriggerShock' + intensity, Type: "Action", Dictionary: Dictionary });
        //ChatRoomMessage({ Content: "TriggerShock" + intensity, Type: "Action", Sender: MemberNumber, Dictionary });

        InventoryShockExpression(player);
    }

    counter: number = 0;

    init(monitor: Monitor) {
        monitor.AddIntervalEvent(5000, (player, delta) => {
            if (this.counter > 0) {
                ChatRoomStdLocalAction(GetString("shock_ctrl_engaged", [`${this.counter}`]));
                CheckOutfitWithSwitch(player, 'ItemNeck').then((i) => {
                    this.single(player, i);
                    return false;
                })

                if (this.counter === 1) {
                    ChatRoomStdLocalAction(GetString("shock_ctrl_finished"));
                }

                this.counter--;
            }
        });
    }
}