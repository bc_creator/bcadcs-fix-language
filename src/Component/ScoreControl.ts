import { GetData, ServerStoreData } from "../Data";
import { ChatRoomStdLocalAction, GetString } from "../Locale";
import { IsInFullOutfit } from "../Outift/OutfitCtrl";
import { Monitor } from "../utils/Monitor";
import { SkillImpair } from "./SkillImpair";

export function IncreaseScore(bonus: number = 1): string {
    if (GetData().ADCS.Punish > 0) {
        GetData().ADCS.Punish -= 1;
        const impairment = Math.round((1 - SkillImpair.instance.ratio) * 100);
        return GetString('score_ctrl_reduce_1_penalty', [`${impairment}`]);
    }
    GetData().ADCS.Score += bonus;
    return GetString('score_ctrl_score_increase', [`${bonus}`]);
}

export function IncreasePunish(): string {
    GetData().ADCS.Punish += 1;
    const impairment = Math.round((1 - SkillImpair.instance.ratio) * 100);
    return GetString('score_ctrl_increase_1_penalty', [`${impairment}`]);
}

export function AwardFullGear(monitor: Monitor) {
    monitor.AddEvent((player, delta) => {
        GetData().ActiveTime.Time += delta;

        const oneHour = 3600000;

        if (IsInFullOutfit(player)) {
            GetData().FullGearTime.Time += delta;
            if (!player.CanInteract())
                GetData().FullGearTime.BoundTime += delta;

            if (GetData().Effects.includes('AwardFullGear') && GetData().FullGearTime.Time > oneHour) {
                GetData().FullGearTime.Time -= oneHour;
                let boundBonus = GetData().FullGearTime.BoundTime > oneHour
                if (boundBonus) {
                    GetData().FullGearTime.BoundTime -= oneHour;
                    ChatRoomStdLocalAction(GetString('award_full_gear_bounded', [IncreaseScore(2)]));
                } else {
                    ChatRoomStdLocalAction(GetString('award_full_gear', [IncreaseScore(1)]));
                }
                ServerStoreData();
            }
        }
    })
}