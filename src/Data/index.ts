const EffectTypeValue = new Set<EffectType>([
    'NoTalkGuide', 'AwardPainInOrgasm',
    'AwardFullGear', 'InjectGGTS',
    'PunishOrgasm', 'BlockRemotes', 'Grounded', 'LoverNotAsAdmin'
]);

export const defaultResponseTime = 30000;

let modData: ModDataType = {};

const Validator = new Map<keyof SolidDataType, (d: ModDataType) => any>([
    ['Theme', (d: ModDataType): SolidDataType['Theme'] => {
        if (d.Theme === undefined) return { Color: '#500410' };
        if (!(/#([0-9A-Z]{6}|[0-9A-Z]{3})/i).test(d.Theme.Color)) return { Color: '#500410' };
        return {
            Color: d.Theme.Color || '#500410',
        };
    }],
    ['ActiveTime', (d: ModDataType): SolidDataType['ActiveTime'] => {
        if (d.ActiveTime === undefined) return { Time: 0 };
        return {
            Time: d.ActiveTime.Time || 0,
        };
    }],
    ['FullGearTime', (d: ModDataType): SolidDataType['FullGearTime'] => {
        if (d.FullGearTime === undefined) return { Time: 0, BoundTime: 0 };
        return {
            Time: d.FullGearTime.Time || 0,
            BoundTime: d.FullGearTime.BoundTime || 0,
        };
    }],
    ['Orgasm', (d: ModDataType): SolidDataType['Orgasm'] => {
        if (d.Orgasm === undefined
            || d.Orgasm.OrgasmTimes === undefined
            || d.Orgasm.RuinedTimes === undefined
            || d.Orgasm.ResistTimes === undefined
            || d.Orgasm.EdgeTime === undefined) {
            return {
                OrgasmTimes: 0,
                RuinedTimes: 0,
                ResistTimes: 0,
                EdgeTime: 0,
            };
        }
        return d.Orgasm;
    }],
    ['Moderator', (d: ModDataType): SolidDataType['Moderator'] => {
        let result: number[] = [];
        if (!Array.isArray(d.Moderator)) return result;
        result = d.Moderator.filter(_ => typeof _ === 'number');
        return result;
    }],
    ['ADCS', (d: ModDataType): SolidDataType['ADCS'] => {
        if (d.ADCS === undefined
            || d.ADCS.Punish === undefined
            || d.ADCS.ResponseTime === undefined
            || d.ADCS.Score === undefined
            || d.ADCS.SelfReference === undefined
            || d.ADCS.OwnerReference === undefined) {
            return {
                Punish: 0,
                ResponseTime: defaultResponseTime,
                Score: 0,
                SelfReference: "",
                OwnerReference: "",
            };
        }
        return d.ADCS;
    }],
    ['Effects', (d: ModDataType): SolidDataType['Effects'] => {
        if (!Array.isArray(d.Effects)) {
            d.Effects = ['InjectGGTS', 'AwardFullGear'];
        }
        let nArr: EffectType[] = [];
        for (let v of d.Effects) {
            if (EffectTypeValue.has(v)) {
                nArr.push(v);
            }
        }
        return nArr;
    }],
])

function EncodeDataStr() {
    let data: { [k: string]: any } = {}
    for (const k in modData) {
        data[k] = modData[k as keyof SolidDataType];
    }
    return LZString.compressToBase64(JSON.stringify(data));
}

function DecodeDataStr(str: string) {
    let d = LZString.decompressFromBase64(str);

    let data = {};
    try {
        let decoded = JSON.parse(d);
        data = decoded;
    } catch { }

    Validator.forEach((v, k) => {
        modData[k as keyof SolidDataType] = v(data);
    })
}

export function ServerStoreData() {
    if (Player && Player.OnlineSettings) {
        ((Player.OnlineSettings as any) as { BCADCSStatus: string }).BCADCSStatus = EncodeDataStr();
    }
    if (ServerAccountUpdate !== undefined && Player !== undefined && Player.OnlineSettings !== undefined) {
        ServerAccountUpdate.QueueData({ OnlineSettings: Player.OnlineSettings });
    }
}

export function ServerTakeData() {
    if (Player && Player.OnlineSettings) {
        DecodeDataStr(((Player.OnlineSettings as any) as { BCADCSStatus: string }).BCADCSStatus);
    }
}

export function GetData() {
    return modData as SolidDataType;
}

export function WriteData(data: SolidDataType) {
    modData = data;
}