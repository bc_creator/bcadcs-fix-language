import bcMod from 'bondage-club-mod-sdk'
import { ExecCommands } from './ChatRoom/Commands';
import { GetData, ServerTakeData } from './Data';
import { ModName, ModVersion } from './Definitions';
import { SelfRefGameChat } from './Component/RefGame';
import { ADCSCheckTask, CheckChat } from './Task/Task';
import { ADCSDrawCharacter } from './Draw';
import { AwardFullGear, IncreasePunish } from './Component/ScoreControl';
import { ChatMessageHandler } from './utils/Chat/ChatMessageHandle';
import { ChatLogHandler } from './utils/Chat/ChatLogHandle';
import { ActivityHandle } from './ChatRoom/Activity';
import { hookOrgasmEvent } from './Component/OrgasmHandle';
import { GetEffect } from './Component/EffectControl';
import { Monitor } from './utils/Monitor';
import { SkillImpair } from './Component/SkillImpair';
import { ShockUnit } from './Component/ShockProvider';
import { NoRelease } from './Component/NoRelease';
import { SendChatModifier } from './utils/Chat/SendChatModifier';
import { DroneTalk } from './Component/DroneTalk';
import { GroundedCtrl } from './Component/Grounded';
import { IsInCollar } from './Outift/OutfitCtrl';
import { ChatRoomStdLocalAction, GetString } from './Locale';

(function () {
    if (window.BCADCS_Loaded) return;

    window.BCADCS_Loaded = false;
    let mod = bcMod.registerMod({ name: ModName, fullName: ModName, version: ModVersion });

    const monitor = new Monitor(200);

    AwardFullGear(monitor);

    SkillImpair.instance.init(mod);

    ShockUnit.instance.init(monitor);

    GroundedCtrl(mod);

    mod.hookFunction('AsylumGGTSGetLevel', 10, (args, next) => {
        if (CurrentScreen === 'Login' || CurrentScreen === 'MainHall') {
            ServerTakeData();
            if (IsInCollar(Player as Character) && GetEffect('InjectGGTS')
                && GetData().ADCS.Punish === 0 && GetData().ADCS.Score > 0) {
                return 0;
            }
        }
        return next(args);
    });

    let initial_notify_not_shown = true;

    mod.hookFunction('ChatRoomRun', 20, (args, next) => {
        next(args);
        if (initial_notify_not_shown) {
            ChatRoomStdLocalAction(GetString("system_loaded", [`${ModVersion}`]));
            initial_notify_not_shown = false;
        }
    });

    const chatMessageHandler = new ChatMessageHandler;
    const chatLogHandler = new ChatLogHandler;

    mod.hookFunction('ChatRoomMessage', 10, (args, next) => {
        next(args);
        chatMessageHandler.Run(Player, args[0] as IChatRoomMessage);
    });

    mod.hookFunction('ChatRoomRun', 10, (args, next) => {
        if (Player) {
            ADCSCheckTask(Player);
            chatLogHandler.Run(Player);
        }
        next(args);
    });

    const chatMod = new SendChatModifier;

    chatMod.register(10, SelfRefGameChat);
    chatMod.register(0, DroneTalk);

    chatMod.init(mod);

    mod.hookFunction('OnlineGameDrawCharacter', 10, (args, next) => {
        if (Player) ADCSDrawCharacter(...(args as [Character, number, number, number]));
        next(args);
    })

    NoRelease(mod);

    chatMessageHandler.Register('Activity', ActivityHandle);

    chatLogHandler.Register((player, sender, msg) => {
        if (player.MemberNumber === sender.MemberNumber) {
            CheckChat(player, msg.Original);
        }
    });

    function ChatRegFilter(player: Character, msg: string) {
        return new RegExp(`^(?:(?:Drone\\s*)?${player.MemberNumber}|#\\s*${CharacterNickname(player)})[\\p{P}\\s~]*(.*)`, 'u').exec(msg);
    }

    chatMessageHandler.Register('Chat', (player, sender, data) => {
        let result = ChatRegFilter(player, data.Content);
        if (!result) return;
        ExecCommands(result[1], player, sender);
    });

    function LoadAndMessage() {
        ServerTakeData();
        console.log(`${ModName} v${ModVersion} ready.`);
    }

    mod.hookFunction('LoginResponse', 20, (args, next) => {
        next(args);
        LoadAndMessage();
    });

    if (Player && Player.MemberNumber) {
        LoadAndMessage();
    }

    hookOrgasmEvent(mod, {
        Orgasm: (C) => { if (GetEffect('PunishOrgasm')) ChatRoomStdLocalAction(GetString('punish_orgasm', [IncreasePunish()])); },
        Resisted: (C) => { },
        Ruined: (C) => { },
    });

    console.log(`${ModName} v${ModVersion} loaded.`);
    window.BCADCS_Loaded = true;
})();
