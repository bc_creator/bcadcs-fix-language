import { ActivityInfo, ChatRoomLocalAction } from "../utils/ChatMessages";
import { IsOwnedBy } from "../utils/Criteria";
import { GetData, ServerStoreData } from "../Data";
import { IncreasePunish, IncreaseScore } from "../Component/ScoreControl";
import { DroneTalkResult } from "../Component/DroneTalk";
import { ChatRoomStdLocalAction, GetString } from "../Locale";

type TaskType = 'Pose' | 'Talk' | 'Activity'

interface ADCSTask {
    Type: TaskType;
    doBonus: boolean;
    doPunish: boolean;
    StartTime: number;
}

interface PoseTask extends ADCSTask {
    Type: 'Pose';
    TargetPose: string[];
}

interface TalkTask extends ADCSTask {
    Type: 'Talk';
    TargetSentence: string,
}

interface ActTask extends ADCSTask {
    Type: 'Activity';
    TargetPlayer: number;
    TargetActivity: string[];
    TargetBodyPart?: string[];
}

let taskList: ADCSTask[] = [];

function CreateContentMsg(guided: boolean, msg: string) {
    if (!guided) return undefined;
    return GetString("task_talk_content", [msg]);
}

function DoBounsMsg(doBonus: boolean) {
    return doBonus ? GetString("task_do_reward_yes") : GetString("task_do_reward_no");
}

function DoPenaltyMsg(doPenalty: boolean) {
    return doPenalty ? GetString("task_do_penalty_yes") : GetString("task_do_penalty_no");
}

export function InitiateTalkTask(msg: string, bonus: boolean, penalty?: boolean) {
    let guided = !GetData().Effects.includes('NoTalkGuide');
    if (penalty === undefined) penalty = !guided;
    ChatRoomStdLocalAction(GetString("task_talk_initiate", [
        [CreateContentMsg(guided, msg), DoBounsMsg(bonus), DoPenaltyMsg(penalty)].filter(_ => _).join(GetString("task_punc_info_sep"))]));
    TaskAdd({
        Type: 'Talk',
        doBonus: bonus,
        doPunish: penalty,
        TargetSentence: msg,
        StartTime: Date.now(),
    })
}

export function InitiatePoseTask(targetPose: string[], bonus: boolean, penalty?: boolean) {
    if (penalty === undefined) penalty = false;
    ChatRoomStdLocalAction(GetString("task_pose_initiate", [
        [DoBounsMsg(bonus), DoPenaltyMsg(penalty)].join(GetString("task_punc_info_sep"))]));
    TaskAdd({
        Type: 'Pose',
        doBonus: bonus,
        doPunish: penalty,
        StartTime: Date.now(),
        TargetPose: targetPose,
    })
}

export function InitiateActTask(act: { TargetPlayer: number, TargetActivity: string[], TargetBodyPart?: string[] }, bonus: boolean, penalty?: boolean) {
    if (penalty === undefined) penalty = false;
    ChatRoomStdLocalAction(GetString("task_act_initiate", [
        [DoBounsMsg(bonus), DoPenaltyMsg(penalty)].join(GetString("task_punc_info_sep"))]));
    TaskAdd({
        Type: 'Activity',
        doBonus: bonus,
        doPunish: penalty,
        StartTime: Date.now(),
        TargetPlayer: act.TargetPlayer,
        TargetActivity: act.TargetActivity,
        TargetBodyPart: act.TargetBodyPart,
    })
}

export function TaskAdd<T extends ADCSTask>(task: T) {
    taskList.push(task);
}

export function CurTaskTime() {
    if (taskList.length > 0) {
        return taskList[0].StartTime;
    }
    return undefined;
}


function TaskSuccMsg(doBonus: boolean) {
    return GetString("task_finish_success", [`${doBonus ? IncreaseScore() : GetString("task_do_reward_no")}`]);
}

function TaskFailMsg(doPunish: boolean) {
    return GetString("task_finish_fail", [`${doPunish ? IncreasePunish() : GetString("task_do_penalty_no")}`]);
}

function CheckTalkTask(msg: string, player: Character, target: string) {
    if (msg === SpeechGarble(player, DroneTalkResult(player, target))) return true;
    else if (msg === DroneTalkResult(player, target)) return true;
    else if (msg === target) return true;
    return false;
}

export function CheckChat(player: Character, msg: string) {
    let updated = false;
    if (taskList.length > 0 && taskList[0].Type === 'Talk') {
        let task = taskList[0] as TalkTask;
        if (CheckTalkTask(msg, player, task.TargetSentence)) {
            ChatRoomStdLocalAction(TaskSuccMsg(taskList[0].doBonus));
            updated = true;
            taskList.shift();
        }
    }
    if (updated) ServerStoreData();
}

export function CheckPose(player: Character, targetPose: string[]) {
    let act = player.ActivePose || ['BaseUpper', 'BaseLower'];
    if (act.some(_ => targetPose.includes(_))) return true;

    if (targetPose.includes('BaseUpper')) {
        return act.every(_a => {
            let d = PoseFemale3DCG.find(_ => _.Name === _a);
            if (d && d.Category === 'BodyLower') return true;
        })
    }

    if (targetPose.includes('BaseLower')) {
        return act.every(_a => {
            let d = PoseFemale3DCG.find(_ => _.Name === _a);
            if (d && d.Category === 'BodyUpper') return true;
        })
    }
}

export function ADCSCheckTask(player: Character) {
    let CurTime = Date.now();
    let updated = false;

    if (taskList.length > 0 && taskList[0].Type === 'Pose') {
        if (CheckPose(player, (taskList[0] as PoseTask).TargetPose)) {
            ChatRoomStdLocalAction(TaskSuccMsg(taskList[0].doBonus));
            updated = true;
            taskList.shift();
        }
    }

    while (taskList.length > 0 && taskList[0].StartTime + GetData().ADCS.ResponseTime < CurTime) {
        ChatRoomStdLocalAction(TaskFailMsg(taskList[0].doPunish));
        updated = true;
        taskList.shift();
    }

    if (updated) ServerStoreData();
}

export function ADCSActivityCheck(player: Character, activity: ActivityInfo) {
    let updated = false;
    if (taskList.length > 0 && taskList[0].Type === 'Activity') {
        let actTsk = (taskList[0] as ActTask);
        if (!actTsk.TargetActivity.includes(activity.ActivityName)) return;
        if (actTsk.TargetPlayer !== activity.TargetCharacter.MemberNumber) return;
        if (actTsk.TargetBodyPart === undefined || actTsk.TargetBodyPart.includes(activity.ActivityGroup)) {
            ChatRoomStdLocalAction(TaskSuccMsg(taskList[0].doBonus));
            updated = true;
            taskList.shift();
        }
    }
    if (updated) ServerStoreData();
}

export function GenerateTalkTask(player: Character, sender: Character, src: (pronoun: string) => string, pronoun: { Deprevated: string, Owner: string, Other: string }) {
    if (PreferenceIsPlayerInSensDep()) {
        InitiateTalkTask(src(pronoun.Deprevated), Math.random() > 0.2, false);
    }
    else {
        if (IsOwnedBy(player, sender)) {
            InitiateTalkTask(src(pronoun.Owner), true, true);
        } else {
            InitiateTalkTask(src(pronoun.Other), Math.random() > 0.2, false);
        }
    }
}