function LockProperties(property: ItemProperty | undefined): { fill: (v: ItemProperty) => void } {
    let ret: ItemProperty = {};
    if (!property || !property.Effect) return { fill: () => { } };
    if (!property.Effect.includes('Lock')) return { fill: () => { } };
    for (const k in property) {
        if (['LockedBy', 'LockMemberNumber', 'CombinationNumber', 'LockPickSeed', 'Password', 'RemoveTimer'].includes(k)) {
            (ret as any)[k] = property[k as keyof ItemProperty]
        }
    }
    return {
        fill: (v: ItemProperty) => {
            if (!v.Effect) {
                v.Effect = ['Lock'];
            }
            else if (!v.Effect.includes('Lock')) {
                v.Effect.push('Lock');
            }
            Object.assign(v, ret);
        }
    }

}

function CompareProperty(p1: any, p2: any) {
    if (Array.isArray(p1) && Array.isArray(p2)) {
        return p1.length === p2.length && p1.every(_ => p2.includes(_));
    }
    return p1 === p2;
}

export function PropertySet(item: Item, property: ItemProperty) {
    if (!item.Property || typeof item.Property !== 'object') {
        item.Property = property;
        return true;
    }

    let lock = LockProperties(item.Property);
    let changed = false;

    if (item.Property.Effect)
        item.Property.Effect = item.Property.Effect.filter(_ => _ !== 'Lock');

    for (const k in property) {
        if (!CompareProperty((item.Property as any)[k], (property as any)[k])) {
            (item.Property as any)[k] = (property as any)[k];
            changed = true;
        }
    }

    lock.fill(item.Property);
    return changed;
}
