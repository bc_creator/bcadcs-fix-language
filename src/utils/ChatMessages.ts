import { ADCS_CUSTOM_ACTION_TAG as CUSTOM_ACTION_TAG } from "../Definitions";

export interface ActivityInfo {
    SourceCharacter: { MemberNumber: number };
    TargetCharacter: { MemberNumber: number };
    ActivityGroup: string;
    ActivityName: string;
}

export function ActivityDeconstruct(dict: ChatMessageDictionary): ActivityInfo | undefined {
    let SourceCharacter, TargetCharacter, ActivityGroup, ActivityName;
    for (let v of dict) {
        if (v.hasOwnProperty('TargetCharacter')) {
            TargetCharacter = { MemberNumber: v['TargetCharacter'] };
        } else if (v.hasOwnProperty('SourceCharacter')) {
            SourceCharacter = { MemberNumber: v['SourceCharacter'] };
        } else if (v.hasOwnProperty('ActivityName')) {
            ActivityName = v['ActivityName'];
        } else if (v.hasOwnProperty('Tag') && v.Tag === 'FocusAssetGroup') {
            ActivityGroup = v['FocusGroupName'];
        }
    }
    if (SourceCharacter === undefined || TargetCharacter === undefined
        || ActivityGroup === undefined || ActivityName === undefined) return undefined;
    return { SourceCharacter, TargetCharacter, ActivityGroup, ActivityName };
}

export function ChatRoomChatMessage(msg: string) {
    if (!msg) return;
    ServerSend("ChatRoomChat", { Content: msg, Type: "Chat" });
}

export function ChatRoomLocalAction(Content: string) {
    if (!Content || !Player || !Player.MemberNumber) return;
    ChatRoomMessage({
        Sender: Player.MemberNumber,
        Content: CUSTOM_ACTION_TAG,
        Type: "Action",
        Dictionary: [
            { Tag: `MISSING PLAYER DIALOG: ${CUSTOM_ACTION_TAG}`, Text: Content },
        ],
    });
}

export function ChatRoomSendAction(Content: string) {
    if (!Content || !Player || !Player.MemberNumber) return;
    ServerSend("ChatRoomChat", {
        Content: CUSTOM_ACTION_TAG,
        Type: "Action",
        Dictionary: [
            { Tag: `MISSING PLAYER DIALOG: ${CUSTOM_ACTION_TAG}`, Text: Content },
        ]
    });
}
