export function FindCharacterByName(name: string | undefined) {
    if (!name) return undefined;
    return ChatRoomCharacter.find(_ => CharacterNickname(_) === name || _.Name === name);
}