export function CharacterName(C: Character) {
    return CharacterNickname(C);
}

export function ReputationSet(RepType: string, value: number) {
    if (!Player || !Player.Reputation) return;
    value = Math.min(100, Math.max(value, 0));
    let rep = Player.Reputation.find(_ => _.Type === RepType);
    if (!rep) Player.Reputation.push({ Type: RepType, Value: value });
    else rep.Value = value;
}